# testui

> 

[![NPM](https://img.shields.io/npm/v/testui.svg)](https://www.npmjs.com/package/testui) [![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)

## Install

```bash
npm install --save testui
```

## Usage

```jsx
import React, { Component } from 'react'

import MyComponent from 'testui'

class Example extends Component {
  render () {
    return (
      <MyComponent />
    )
  }
}
```

## License

MIT © [jkos](https://github.com/jkos)
